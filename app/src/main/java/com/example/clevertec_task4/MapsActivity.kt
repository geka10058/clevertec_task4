package com.example.clevertec_task4

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.clevertec_task4.data.ATM
import com.example.clevertec_task4.data.Controller
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.example.clevertec_task4.databinding.ActivityMapsBinding

class MapsActivity : AppCompatActivity(), OnMapReadyCallback {

    val controller = Controller()
    var liveDataATM = controller.liveDataATM

    private lateinit var mMap: GoogleMap
    private lateinit var binding: ActivityMapsBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMapsBinding.inflate(layoutInflater)
        setContentView(binding.root)

        val mapFragment = supportFragmentManager
            .findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)
    }

    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap
        liveDataATM.observe(this) {
            it.forEach {
                addMarker(it)
            }
        }
        val gomel = LatLng(52.430719, 30.990817)
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(gomel, 12f))
        mMap.uiSettings.apply {
            isZoomControlsEnabled = true
        }
    }

    private fun addMarker(atm: ATM){
        val position = LatLng(atm.gpsX.toDouble(), atm.gpsY.toDouble())
        val fullAddress = "${atm.addressType} ${atm.address} ${atm.house}"
        mMap.addMarker(MarkerOptions().position(position).title(fullAddress))
        //Log.d("TAG", "${fullAddress}")
    }


}