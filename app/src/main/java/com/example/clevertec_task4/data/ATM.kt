package com.example.clevertec_task4.data

import com.google.android.gms.maps.model.LatLng
import com.google.gson.annotations.SerializedName
import com.squareup.moshi.Json

data class ATM(
    @field:Json(name = "id")
    val id: String,
    @field:Json(name = "address_type")
    val addressType: String,
    @field:Json(name = "address")
    val address: String,
    @field:Json(name = "house")
    val house: String,
    @field:Json(name = "gps_x")
    val gpsX: String,
    @field:Json(name = "gps_y")
    val gpsY: String,
)

