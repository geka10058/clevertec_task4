package com.example.clevertec_task4.data

import androidx.lifecycle.MutableLiveData
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Headers
import retrofit2.http.Query

interface BBAPI {
    @Headers("Content-Type: application/json")
    @GET("atm")
    fun searchATM(
        @Query("city") city: String = "Гомель"
    ): Call <List<ATM>>
}