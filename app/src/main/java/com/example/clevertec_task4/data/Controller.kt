package com.example.clevertec_task4.data

import androidx.lifecycle.MutableLiveData
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory

class Controller : Callback<List<ATM>> {
    lateinit var atmList: List<ATM>
    var liveDataATM = MutableLiveData<List<ATM>>()

    init {
        downloadData()
    }

    private fun downloadData() {

        val retrofit = Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(MoshiConverterFactory.create())
            .build()

        val bbApi: BBAPI = retrofit.create(BBAPI::class.java)
        val call: Call<List<ATM>> = bbApi.searchATM()
        call.enqueue(this)
    }

    override fun onResponse(call: Call<List<ATM>>, response: Response<List<ATM>>) {
        if (response.isSuccessful) {
            atmList = response.body()!!
            liveDataATM.postValue(atmList)
        } else {
            println(response.errorBody())
        }
    }

    override fun onFailure(call: Call<List<ATM>>, t: Throwable) {
        t.printStackTrace()
    }

}

const val BASE_URL = "https://belarusbank.by/api/"